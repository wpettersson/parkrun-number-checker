#!/usr/bin/env python3

import base64
import fileinput
import hashlib
import re

BUF_SIZE = 65536
JS_FILES = ["js/number-checker.js"]
def makesri(js):
    hash384 = hashlib.sha384()
    with open(js, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            hash384.update(data)
    hash384 = hash384.digest()
    hash_base64 = base64.b64encode(hash384).decode('utf-8')
    return f'sha384-{hash_base64}'



for line in fileinput.input():
    for js in JS_FILES:
        line = re.sub(f'SRI-{js}', makesri(js), line)
    print(line.rstrip())
