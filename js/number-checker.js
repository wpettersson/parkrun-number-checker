
var time;
var startTime;
var timerInt = null;
let container = document.querySelector("#timerContainer");
var timer = container.querySelector("div#timer");
var startBtn = container.querySelector("button#btn-start");
var resetBtn = container.querySelector("button#btn-reset");
let addBtn = document.querySelector("button#btn-add");

console.log(startBtn);
startBtn.onclick = function(e) {
	console.log("start");
	startTime = Date.now()/1000;
	console.log("Start time is " + startTime);
	timerInt = setInterval(function() {
		time = (Date.now()/1000) - startTime;
		var minutes = Math.floor(time/60);
		var seconds = Math.round(time - 60*minutes, 0);
		timer.innerHTML=minutes.toString().padStart(2, "0") + ":" + seconds.toString().padStart(2, "0");
	}, 1000);
	startBtn.disabled = true;
	resetBtn.disabled = false;
	addBtn.disabled = false;
};

let table = document.querySelector("table#inputTable");
resetBtn.onclick = function(e) {
	console.log("reset");
	if (timerInt) {
		clearInterval(timerInt);
	}
	time = 0;
	timer.innerHTML='00:00';
	startBtn.disabled = false;
	resetBtn.disabled = true;
	addBtn.disabled = true;
	while (table.querySelectorAll("tr").length > 1) {
		table.deleteRow(-1);
	}
};

function addRow() {
	var row = table.insertRow(-1);
	var cell = row.insertCell(0);
	var minutes = Math.floor(time/60);
	var seconds = Math.round(time - 60*minutes, 0);
	cell.innerHTML = minutes.toString().padStart(2, "0") + ":" + seconds.toString().padStart(2, "0");
	var cell = row.insertCell(1);
	cell.setAttribute("contenteditable", "true");
	cell.setAttribute("inputmode", "numeric");
	cell.setAttribute("pattern", "[0-9]*");
	cell = row.insertCell(2);
	cell.setAttribute("contenteditable", "true");
	cell.setAttribute("inputmode", "numeric");
	cell.setAttribute("pattern", "[0-9]*");
	cell = row.insertCell(3);
	cell.setAttribute("contenteditable", "true");
	cell.setAttribute("inputmode", "numeric");
	cell.setAttribute("pattern", "[0-9]*");
	cell = row.insertCell(3);
	cell.setAttribute("contenteditable", "true");
}


addBtn.onclick = addRow;
