default: deploy

clean:
	@rm -rf build/

build:
	mkdir -p build
	cp -r css js build/
	./set_sri.py index.html > build/index.html

deploy: clean build
	rsync -avr build/* ewpettersson.se:ewpettersson.se/public/pnc/
